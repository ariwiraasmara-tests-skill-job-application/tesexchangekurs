<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left" style="margin-top: 17.5px;">
        <i class="ion-android-menu txt-white"></i>
    </div>
    <span class="title txt-white bold" style="font-size: 25px;">Dashboard</span>
</div>

<div class="animated fadeinup">
	<div class="txt-black p-20">

        <div class="row">

            <div class="col s10 animated" style="border-bottom: 1px solid #ccc;">
                <h3 class="right txt-black" id="toBTC"></h3>
            </div> 
            <div class="col s2 animated" style="border-bottom: 1px solid #ccc;"><h3 class="bold txt-black">BTC</h3></div>
            <input type="hidden" id="convBTC" value="-btc" />

             <div class="col s10 animated" style="border-bottom: 1px solid #ccc;">
                <h3 class="right txt-black" id="toETH"></h3>
            </div> 
            <div class="col s2 animated" style="border-bottom: 1px solid #ccc;"><h3 class="bold txt-black">ETH</h3></div>
            <input type="hidden" id="convETH" value="-eth" />

             <div class="col s10 animated" style="border-bottom: 1px solid #ccc;">
                <h3 class="right txt-black" id="toBNB"></h3>
            </div> 
            <div class="col s2 animated" style="border-bottom: 1px solid #ccc;"><h3 class="bold txt-black">BNB</h3></div>
            <input type="hidden" id="convBNB" value="-bnb" />

             <div class="col s10 animated" style="border-bottom: 1px solid #ccc;">
                <h3 class="right txt-black" id="toUSD"></h3>
            </div> 
            <div class="col s2 animated" style="border-bottom: 1px solid #ccc;"><h3 class="bold txt-black">USD</h3></div>
            <input type="hidden" id="convUSD" value="-usd" />

             <div class="col s10 animated" style="border-bottom: 1px solid #ccc;">
                <h3 class="right txt-black" id="toIDR"></h3>
            </div> 
            <div class="col s2 animated" style="border-bottom: 1px solid #ccc;"><h3 class="bold txt-black">IDR</h3></div>
            <input type="hidden" id="convIDR" value="-idr" />

        </div>

        <div class="row">
            <div class="col s4">
                <div class="input-field">
                    <label for="exval" class="txt-black">Nilai</label>
                    <input type="number" id="nilai" class="validate txt-black" />
                </div>
            </div>

            <div class="col s4 m-t-30">
                <select class="border-select browser-default txt-black" id="src">
                    <option value="" disabled selected>-- Pilih Asal Mata Uang --</option>
                    <option value="btc">BTC</option>
                    <option value="eth">ETH</option>
                    <option value="bnb">BNB</option>
                    <option value="usd">USD</option>
                    <option value="idr">IDR</option>
                </select>
            </div>

            <div class="col s4 m-t-30">
                <button class="btn borad-10 primary-color txt-white width-100 waves-effect waves-light" id="btn_ok">OK</button>
            </div>

            <div class="col s12 m-t-30">
                <span class="bold italic">References API Source :</span><br/>
                <a href="https://www.cryptonator.com/api" class="txt-link italic">https://www.cryptonator.com/api/ticker/{kode_mata_uang_asal-kode_mata_uang_target}</a>
            </div>
        </div>

    </div>
</div>

<script src="js/functions/cryptocurrency.js"></script>
<script>
    $(document).ready(function(){
        setInterval(onProcess('btc', 1), 3600000);
        $("#btn_ok").click(function(){
            let src_kode = $("#src :selected").val();
            let nilai    = $("#nilai").val();
            onProcess(src_kode, nilai);
        });
    });
</script>