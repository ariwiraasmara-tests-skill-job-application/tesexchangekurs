<ul id="slide-out-left" class="side-nav collapsible georgia">
	<li>
		<div class="sidenav-header primary-color">
			<div class="nav-avatar">
                <a href="#viewfoto" class="modal-trigger scroll-hide" style="height: 100px;"><img class="circle avatar" src="images/me.jpg" alt="Syahri Ramadhan Wiraasmara"></a>

			    <div class="avatar-body">
                    <h5 class="txt-white">
						<span class="bold">Syahri Ramadhan Wiraasmara</span>
					</h5>
				</div>
			</div>
		</div>
	</li>

	<li><a href="?" class="no-child txt-black"><i class="ion-android-home"></i> Dashboard</a></li>
	<li><a href="?pg=profil" class="no-child txt-black"><i class="ion-person"></i> Profil</a></li>
	<li class="bold"><a href="#copyright" class="no-child txt-black center">Copyright © Syahri Ramadhan Wiraasmara</a></li>
</ul>