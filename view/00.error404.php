<div id="toolbar" class="primary-color">
    <a href="?" class="open-left" style="margin-top: 17.5px;">
        <i class="ion-android-arrow-back txt-white"></i>
    </a>
    <span class="title txt-white bold" style="font-size: 25px;">Error 404!</span>
</div>

<div class="animated fadeinup scroll-hide">
	<div class="georgia width-100">
        <h1 class="center bold italic txt-black m-t-50">
            Halaman Tidak Ditemukan!
        </h1>
    </div>
</div>