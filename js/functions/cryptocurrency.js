function getData(kode) {
    var result = null;
        $.ajax({
            url: "https://api.cryptonator.com/api/ticker/"+kode,
            type: 'GET',
            dataType: 'JSON',
            async: false,
            success: function(data) {
                result = data;
            }
        });
    return result;
}

function onProcess(src_kode, nilai) {
    //alert('ok');
    const btc = $("#convBTC").val();
    const eth = $("#convETH").val();
    const bnb = $("#convBNB").val();
    const usd = $("#convUSD").val();
    const idr = $("#convIDR").val();

    const kode_btc = src_kode + btc;
    const kode_eth = src_kode + eth;
    const kode_bnb = src_kode + bnb;
    const kode_usd = src_kode + usd;
    const kode_idr = src_kode + idr;
    //alert(kode_btc);

    const priceBTC = getData(kode_btc);
    const priceETH = getData(kode_eth);
    const priceBNB = getData(kode_bnb);
    const priceUSD = getData(kode_usd);
    const priceIDR = getData(kode_idr);

    const targetBTC = (src_kode == 'btc' ? nilai : nilai * priceBTC.ticker.price);
    const targetETH = (src_kode == 'eth' ? nilai : nilai * priceETH.ticker.price);
    const targetBNB = (src_kode == 'bnb' ? nilai : nilai * priceBNB.ticker.price);
    const targetUSD = (src_kode == 'usd' ? nilai : nilai * priceUSD.ticker.price);
    const targetIDR = (src_kode == 'idr' ? nilai : nilai * priceIDR.ticker.price);

    const finalBTC = parseFloat(targetBTC).toLocaleString('id-ID', { 
                        minimumFractionDigits: 7,
                        maximumFractionDigits: 7 
                    });

    const finalETH = parseFloat(targetETH).toLocaleString('id-ID', { 
                        minimumFractionDigits: 7,
                        maximumFractionDigits: 7 
                    });

    const finalBNB = parseFloat(targetBNB).toLocaleString('id-ID', { 
                        minimumFractionDigits: 7,
                        maximumFractionDigits: 7 
                    });

    const finalUSD = parseFloat(targetUSD).toLocaleString('id-ID', { 
                        minimumFractionDigits: 7,
                        maximumFractionDigits: 7 
                    });

    const finalIDR = parseFloat(targetIDR).toLocaleString('id-ID', { 
                        minimumFractionDigits: 7,
                        maximumFractionDigits: 7 
                    });

    //console.log(finalBTC + ' BTC');
    //console.log(finalETH + ' ETH');
    //console.log(finalBNB + ' BNB');
    //console.log(finalUSD + ' USD');
    //console.log(finalIDR + ' IDR');
    $("#toBTC").html(finalBTC);
    $("#toETH").html(finalETH);
    $("#toBNB").html(finalBNB);
    $("#toUSD").html(finalUSD);
    $("#toIDR").html(finalIDR);
}