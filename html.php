<html>
    <head>
        <title>Tes Exchange Kurs Cryptography | Syahri Ramadhan Wiraasmara</title>
        <meta content="IE=edge" http-equiv="x-ua-compatible">
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
        <meta content="yes" name="apple-mobile-web-app-capable">
        <meta content="yes" name="apple-touch-fullscreen">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        
        <!-- Icons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
        
        <!-- Styles -->
        <link href="css/default/keyframes.css" rel="stylesheet" type="text/css">
        <link href="css/default/materialize.min.css" rel="stylesheet" type="text/css">
        <link href="css/default/swiper.css" rel="stylesheet" type="text/css">
        <link href="css/default/swipebox.min.css" rel="stylesheet" type="text/css">
        <link href="css/default/style.css" rel="stylesheet" type="text/css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="js/functions/cryptocurrency.js"></script>
    </head>

    <body>
        <div class="m-scene" id="main">
            <div id="content" class="page">
                <?php
                require("view/sidebar.php");
                require("view.php");
                ?>

                <div class="modal borad-20" id="viewfoto" style="background: rgba(200, 250, 255, 0.7);">
					<img height="50" class="modal-close" src="images/me.jpg" alt="Syahri Ramadhan Wiraasmara">
				</div>
            </div>
        </div>

        <script src="js/default/jquery-2.1.0.min.js"></script>
        <script src="js/default/jquery.swipebox.min.js"></script>   
        <script src="js/default/materialize.min.js"></script> 
        <script src="js/default/swiper.min.js"></script>  
        <script src="js/default/jquery.mixitup.min.js"></script>
        <script src="js/default/masonry.min.js"></script>
        <script src="js/default/chart.min.js"></script>
        <script src="js/default/functions.js"></script>
    </body>
</html>