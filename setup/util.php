<?php
class utilfunction {

    public function __construct() {

    }

    public function POST(String|float $str=null) {
        return strtolower(@$_POST[$str]);
    }
    
    
    public function GET(String|float $str=null) {
        return strtolower(@$_GET[$str]);
    }
    
    public function FILE(String|float $str=null, String $tipe=null) {
        return @$_FILES[$str][$tipe];
    }

    // SET TITLE
    public $title; public $project_title; public $separator_title;
    public function setTitle(String|float $title = null) {
        try {
            $this->title = $title;
        }
        catch(Exception $e) {
            echo "function setTitle() Error: ".$e;
        }
    }

    public function setProjectTitle(String|float $title = null) {
        try {
            $this->project_title = $title;
        }
        catch(Exception $e) {
            echo "function setProjectTitle() Error: ".$e;
        }
    }

    public function setSepatatorTitle(String $title = null) {
        try {
            $this->separator_title = $title;
        }
        catch(Exception $e) {
            echo "function setSepatatorTitle() Error: ".$e;
        }
    }
    
    public function getTitle() {
        try {
            return $this->title;
        }
        catch(Exception $e) {
            return "function getTitle() Error: ".$e;
        }
    }

    public function getProjectTitle() {
        try {
            return $this->project_title;
        }
        catch(Exception $e) {
            return "function getProjectTitle() Error: ".$e;
        }
    }

    public function getSeparatorTitle() {
        try {
            return $this->separator_title;
        }
        catch(Exception $e) {
            return "function getSeparatorTitle() Error: ".$e;
        }
    }
    
    public function getDocTitle(String|float $title = "__-__") { ?>
        <script type="text/javascript">
            document.title = "<?php echo $this->getTitle().$this->getSeparatorTitle().$this->getProjectTitle(); ?>";
            $("<?php echo '#'.$title; ?>").html("<?php echo $this->getTitle(); ?>");
        </script>
        <?php
    }

    public function formatnumber(int|float $str=null, $koma=0) {
        return number_format($str, $koma, ',', '.');
    }
    
}
?>