<?php
class cryptocurrency {

    public function __construct(String $kode = null) {
        $this->hitAPI($kode);
    }

    public function formatnumber(int|float $str=null, $koma=0) {
        return number_format($str, $koma, ',', '.');
    }

    protected $json_result_currency;
    public $price;
    public function hitAPI(String $kode) {
        // $kode => btc-usd, btc-idr, btc-eth, btc-bnb,  
        // $kode => eth-usd, eth-idr, eth-btc, eth-bnb,
        // $kode => bnb-usd, bnb-idr, bnb-bth, bnb-eth

        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL, "https://api.cryptonator.com/api/ticker/".$kode);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //curl_setopt($ch,CURLOPT_HEADER, false); 
    
        $output = curl_exec($ch);
        curl_close($ch);
        //return $output;
        //return array_values(json_decode($output, true));
        $this->setData($output);
    }

    public function setData($number) {
        $this->json_result_currency = array_values(json_decode($number, true));
        $this->price = $this->formatnumber($this->json_result_currency[0]["price"], 7);
    }

    
    public function price() {
        return $this->formatnumber($this->json_result_currency[0]["price"], 5);
        //$this->price = $this->formatnumber($this->json_result_currency[0]["price"], 2);
    }

    //https://de.cryptonator.com/api
    //http://hayageek.com/php-curl-post-get/
}
?>